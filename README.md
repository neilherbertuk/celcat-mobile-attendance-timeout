------------------------------------
- Celcat Mobile Attendance Timeout -
------------------------------------

Written by Neil Herbert
Written in Javascript with jQuery 1.9.1 and jQuery Mobile 1.3.1 but works with newer versions.

This plug-in is for the Celcat Mobile Attendance Utility - http://www.celcat.com/technical-support/system-requirements/utilities/celcat-mobile-web-attendance.aspx. 
It has been written in JavaScript and makes use of the already included jQuery and jQuery Mobile libraries. Installation requires a single line of code to be added to the Celcat Mobile Attendance Utility.

The Celcat Mobile Attendance Utility does not currently have any built-in security features, when used on publicly accessible kiosks this adds a potential data protection issue if the user does not logout once finished. This plugin has been developed to introduce an idle timeout. After 5 minutes of inactivity a warning box appears on screen with an additional, visible 30 second countdown. If the user fails to interact with the screen within the 30 second countdown, the user gets logged out.

This code can easily be modified for use with other systems, the demo supplied has had any functionality specific to Celcat removed. Just modify the logout() function to suit ;
your needs.

-----------
- License -
-----------

Distributed under MIT License

The following terms apply as long as you attribute the source and copyright notice of the author

- You are free to use and modify the code provided
- You may use the code for any purpose including commerical gain
- You are free to re-distribute the code as long as it is done under MIT License
- You may not hold the author liable for any damages caused from using this code

For the full license please see the LICENSE.MD text file.

--------
- Demo -
--------

An online demo is available at - http://neilherbertuk.bitbucket.org/CelcatDemo/ - This does not host anything related to Celcat but shows the script in action with any Celcat related function removed.

The demo has also been provided in the Demo folder, this has been modified to remove any Celcat specific functionality. Please do not use this version for the Celcat Mobile Attendance Utility.

The demo is set to timeout after 20 seconds followed by a 10 second count down.

----------------
- Instructions -
----------------

1. Copy the supplied idle.js file to the Scripts folder within the Celcat Mobile Attendance root folder.

2. Open Site.Master in notepad (you might need admin rights)

3. Insert the following on a new line after <script src="./Scripts/jquery-1.9.1.min.js"></script>. (The version number for jQuery might differ from that documented here.)

	<script src="./Scripts/idle.js"></script>

4. Save the file

5. Test by opening the Celcat Mobile Attendance Utility in your browser, login and wait until you receive the idle warning popup after 5 minutes of being idle.

Please note that this timeout script will need to be reinstall when you update your installation of the Celcat Mobile Attendance Utility.

The default idle timeout before the user is shown the warning is 5 minutes, followed by a 30 second count down. These values can be changed at the top of the idle.js file

	idleTimeout - Seconds session requires to be idle before showing idle warning dialog
	warningTimeout - Seconds to display the idle warning dialog before taking action

------------
- Versions -
------------
Tha latest version under development will always be located on Source

Completed / released versions will be available via the Downloads section.