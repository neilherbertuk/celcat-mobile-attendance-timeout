/********************************************************************************************************
 *
 * THIS IS A DEMO ONLY AND HAS BEEN MODIFIED SO WILL NOT WORK WITH THE CELCAT MOBILE ATTENDANCE UTILITY
 * PLEASE DO NOT USE THIS IDLE.JS FOR ANY OTHER PURPOSE
 *
 ********************************************************************************************************/

$(function () {
// Initialise variables
var idleTimeout = 20, // Seconds session requires to be idle before showing idle warning dialog
	warningTimeout = 10, // Seconds to display the idle warning dialog before taking action
	// Do not edit below this line
	checkTime, status = 1, timeOut, counterRefresh, idleCounter = 0,
	title = document.title, anchor = (document.URL.split('?').length > 1) ? document.URL.split('?')[1] : null,
	page = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);

// Start Program
registerEvents();
checkTimeout();

// Functions

/**
 * @name:	checkTimeout
 * @description: Called every 1 second to increase idleCounter
 *		 and check if the warning needs to be shown
 */

function checkTimeout(){
	if(idleCounter == idleTimeout && status == 1){
		// Idle counter has hit total show warning
		showWarning();
	} else {
		// Increase idleCounter
		idleCounter++;
	}
	if (status == 1){
		// Re-run checkTimeout function
		checkTime = setTimeout(arguments.callee, 1000);// restart timer
	}
}// End checkTimeout

/**
 * @name:	 createStyles
 * @description: Adds CSS Styling to warning box
 */

function createStyles(){

	// Get page width and height
	var pageHeight = $(document).height();
	var pageWidth = $(document).width();

	$(".darkenPage").css({
		"position" : "absolute",
		"top" : "0",
		"left" : "0",
		"background-color" : "rgba(0,0,0,0.6)",
		"height" : pageHeight,
		"width" : pageWidth,
		"z-index" : "10"
	});

	$(".warningBox").css({
		"position" : "absolute",
		"font-family" : "Helvetica,Arial,sans-serif",
		"top" : "50%",
		"left": "50%",
		"margin-top" : "-133px",
		"margin-left" : "-250px",
		"display" : "none",
		"height" : "266px",
		"width" : "500px",
		"background-color" : "#fff",
		"border" : "1px solid #333",
		"box-shadow" : "0px 2px 7px #292929",
		"-moz-box-shadow": "0px 2px 7px #292929",
		"webkit-box-shadow" : "0px 2px 7px #292929",
		"border-radius" : "4px",
		"-moz-border-radius" : "4px",
		"-webkit-border-radius" : "4px",
		"z-index" : "50"
	});
	$(".warningBox H2").css({
		"text-align" : "center",
		"width" : "100%"
	});

	$(".warningBox #counter").css({
		"font-weight" : "bold"
	});
	
	$("p.center").css({
		"text-align" : "center"
	});
	
	$("p.bold").css({
		"font-weight" : "bold"
	});
	
	$(".modalheader").css({
		"background-image" : "linear-gradient(to top , #111, #3C3C3C)",
		"background" : "-webkit-gradient(linear, center top, center bottom, from(#3C3C3C), to(#111))",
		"background" : "linear-gradient(#3C3C3C, #111) repeat scroll 0% 0% #111",
		"background" : "-webkit-linear-gradient(#3C3C3C, #111) repeat scroll 0% 0% #111",
		"background" : "-moz-linear-gradient(#3C3C3C, #111) repeat scroll 0% 0% #111",
		"text-shadow" : "0px -1px 0px #000",
		"border" : "1px solid #333",
		"color" : "#fff",
		"padding" : "18px 18px 14px",
		"border-bottom" : "1px solid #CCC",
		
	});
	
	$(".btn").css({
		"position" : "absolute",
		"display" : "inline-block",
		"left": "50%",
		"width" : "200px",
		"margin-left" : "-100px",
		"margin-bottom" : "0px",
		"font-weight" : "400",
		"text-align" : "center",
		"vertical-align" : "middle",
		"cursor" : "pointer",
		"background-image" : "none",
		"border" : "1px solid transparent",
		"white-space" : "nowrap",
		"padding" : "6px 12px",
		"font-size" : "16px",
		"line-height" : "1.42857",
		"border-radius" : "4px",
		"-moz-user-select" : "none",
		"color" : "#FFF",
		"background-color" : "#333",
		"border-color" : "#333"
	});

} // End createStyles

/**
 * @name:	extendCounter
 * @description: Resets idleCounter to 0 when called
 *		 Also removes warning box if shown
 */

function extendCounter(){

	// Re-set idleCounter
	idleCounter = 0;

	// Is Idle warning is open close it
	if (timeOut !== undefined || timeOut !== null){
		clearInterval(checkTime);
		status = 1;
		$(".warningBox").fadeOut();
		$(".darkenPage").fadeOut();
		$(".warningBox").remove();
		$(".darkenPage").remove();
		document.title = title;
		checkTimeout();
	}
	
}// End extendCounter

/**
 * @name:		logout
 * @description: Logs user out
 */

function logout(){
	status = 3;
	// Redirect to default.aspx page
	$(location).attr('href','./loggedout.html');
}

/**
 * @name:        registerEvents
 * @description: Binds user interaction events
 */

function registerEvents(){
    $("*").on("tap", extendCounter );
    $("*").on("taphold", extendCounter );
    $("*").on("click", extendCounter );
    $("*").on("swipe", extendCounter );
    $("*").on("scrollstart", extendCounter );
    $("*").on("scrollstop", extendCounter );
    $("*").on("touchstart", extendCounter );
    $("*").on("mousemove", extendCounter );
    $("*").on("keypress", extendCounter );
}// End registerEvents

/**
 * @name 	showWarning
 * @description	Displays the Warning dialog to user and begins a countdown before logging user out.
 */
function showWarning(){

	timeOut = warningTimeout;
	clearInterval(checkTime);
	idleCounter = 0;
	status = 2;
	document.title = title + " - idle";
	$("<div class=\"darkenPage\"></div>").appendTo("body");
	$("<div class=\"warningBox\"><div class=\"modalheader\"><h2>Your Session is About to Expire</h2></div><br /><p class=\"center\">Your session has been inactive for too long,<br />you will be logged out in <span id=\"counter\">" + timeOut + "</span> seconds.</p><br /><div class=\"center bold btn\">Tap to Continue</div></div>").appendTo(".darkenPage");
	createStyles();
	$(".warningBox").fadeIn();

	// Logout Timer
	counterRefresh = setTimeout(function(){
		if(timeOut <= 0){
		// Log user out
		$(".warningBox").html("<h2>Logging Out</h2>");
			timeOut = null;
			clearInterval(counterRefresh);
			if (status != 3){
				logout();
			}
		}
		// Update timer
		$(".warningBox #counter").html(timeOut);
		timeOut--;
		
		// Restart counter loop
		if (status == 2){ counterRefresh = setTimeout(arguments.callee, 1000); }// restart timer
	}, 1000);

}// End showWarning

});